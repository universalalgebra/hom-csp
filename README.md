# hom-csp

Research on computational complexity of problems involving existence (or counting the number) of homomorphisms of algebraic, relational, and general structures.

**Authors**. Libor Barto, William DeMeo, Antoine Mottet

## Compiling the paper

Use the command

```
xelatex main.tex
bibtex main.aux
xelatex main.tex
xelatex main.tex
```

to compile the document `main.pdf`.

## Contributing to this repository

If you wish to contribute to this repository, the best way is to use the standard [fork-clone-pull-request](https://gist.github.com/Chaser324/ce0505fbed06b947d962) workflow described [here](https://gist.github.com/Chaser324/ce0505fbed06b947d962). If you identify an issue that should be addressed by contributors to this repository, please open a new issue.

Thanks for your interest in our work!

---------------------

COPYRIGHT: (C) 2020 Barto, DeMeo, Mottet

ALL RIGHTS RESERVED

