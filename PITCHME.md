## The Homomorphism Problem
### for Boolean structures

[William DeMeo &lt;williamdemeo@gmail.com&gt;](mailto:williamdemeo@gmail.com)

joint work with
[Libor Barto](mailto:libor.barto@gmail.com)
& [Antoine Mottet](https://orcid.org/0000-0002-3517-1745)
<br><br>
<a style="color:#e7ad52">The Opršal CSP Zoom Lecture Series</a><br>
<br>
7 Oct 2020

<!-- <a style="color:#e7ad52"> -->

<br>
<br>
<br>

Slides available at https://gitpitch.com/UniversalAlgebra/hom-csp?grs=gitlab#/1

---

## A new dichotomy result

Let 𝑆 be an arbitrary finite signature.
<br>
<br>
For a fixed Boolean 𝑆-structure ℬ, the problem of deciding
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp; Given an 𝑆-structure 𝒜, is there a homomorphism from 𝒜 to ℬ?
<br>
<br>
 
is either in P or NP-complete.

---

## Background and Motivation
### toward a general algebraic approach
<br>

@ul

- We study a generalization of finite-template CSP over an arbitrary finite structure.<br><br>
- We allow both relation symbols *and operation symbols* in the signature.<br><br>
- We generalize Schaefer's dichotomy theorem from *relational* Boolean structures to *arbitrary* Boolean structures.<br><br>
- ...still, a general theory for CSPs over arbitrary structures is missing. 

@ulend

---

## Main Result

A generalization of the Schaefer dichotomy to structures with relations *and operations*.<br><br>

<p class="fragment" align="left"> 
**Theorem**.<br><br>
If ℬ is a Boolean structure with finitely many relations and operations, then CSP(ℬ) is in P or is NP-complete.
</p>
<br>

<p class="fragment" align="left"> 
To state our result precisely, we need a defintion.
</p>
<br>
<p class="fragment" align="left"> 
If 𝑓(𝑥₁, …, 𝑥ₙ) is a function in 𝑛 variables, then 𝑓 &nbsp;is *essentially unary*&nbsp; if ∃ 𝑖 and a (possibly constant) function 𝑔 such that 𝑓(𝑥₁, …, 𝑥ᵢ, …, 𝑥ₙ) = 𝑔(𝑥ᵢ).<br><br>
</p>

<p class="fragment" align="left"> 
**Theorem**.<br><br>
If ℬ is a Boolean structure, then<br><br>

@ul

- CSP(ℬ) is in P if ℬ has an operation that is not essentially unary or ∃ 𝑝 ∈ Pol(𝒢(ℬ)) that is constant or not essentially unary.<br><br>

- If all functions in ℬ are essentially unary, then CSP(ℬ) is equivalent to CSP(𝒢(ℬ)).

@ulend

---

### In fact, the result is a bit stronger...

<p class="fragment" align="left">
We show, under the above conditions on ℬ:
</p>
<br>

<p class="fragment" align="left">
There is at most a polynomial number of homomorphisms from each CSP(ℬ) instance to ℬ, and
</p>
<br>

<p class="fragment" align="left">
they can be effectively enumerated.  
</p>
<br>

<p class="fragment" align="left">
Thus, we have a poly-time algorithm for CSP(ℬ).
</p>

---

## Prior Art

The only prior work we know of is the paper by Feder, Madelaine, Stewart:
<br>
<br>
[Dichotomies for classes of homomorphism problems involving unary functions](http://www.sciencedirect.com/science/article/pii/S0304397503006479)
<br>
<br>
<i>Theoretical Computer Science</i>, 314 (1), 2004.
<br>
<br>

<p class="fragment" align="left"> 
They show the purely algebraic CSP generalizes the relational one in the following sense:<br><br>
Each CSP of a finite relational structure is equivalent to a CSP of a finite algebraic structure modulo poly-time Turing reductions.
</p>
<br>

<p class="fragment" align="left"> 
In general there's no poly-time reduction from CSP(𝒢(ℬ)) to CSP(ℬ).
</p>
<br>
<p class="fragment" align="left"> 
However, FMS prove the following reduction from CSP(𝒢(ℬ)) to CSP(ℬ) when all operations of ℬ are *essentially unary*:
</p>
<br>
<p class="fragment" align="left">
*FMS Theorem*<br>
<br>
For a structure ℬ whose operation symbols are at most unary, CSP(ℬ) is equivalent to CSP(𝒢(ℬ)) via poly-time many-one reductions.
<br><br>

<p class="fragment" align="left">
This theorem does not extend to structures with operations of higher arity.<br><br>
Later, we give an example of a two element algebra with a single binary operation for which it fails.
</p>

---

## Some background

The border between P and NP-complete in Schaefer's dichotomy theorem can be described in terms of polymorphisms.<br><br>

<p class="fragment" align="left">
For a binary relational structure ℬ

@ul

- CSP(ℬ) is in P if ℬ has a polymorphism that is constant or not essentially unary.
- Otherwise, it is NP-complete.

@ulend
<br>
<br>

<p class="fragment" align="left">
The discovery of the role of polymorphisms for relational CSPs prompted significant improvements; e.g., Krokhin (2005), Barto (2018).<br><br>
</p>

<p class="fragment" align="left">
These made it possible to conjecture a borderline between P and NP-complete CSPs over non-Boolean domains and were important for further generalizations to

@ul

- infinite domains: Bodirsky (2006)<br>
- promise CSPs: Austrin, Guruswami, Haastad (2017), Brakensiek (2016), Bulin (2019)<br>
- valued CSPs: Cohen (2013)

@ulend

</p>
<br>
<br>
<p class="fragment" align="left">
We do not have analogues of these results in the non-relational setting...
</p>
<p class="fragment" align="left">
<i>...future work!</i>
</p>

---

## Reductions

<p class="fragment" align="left">
**Fact**. A map ℎ : 𝒜 → ℬ is a homomorphism iff it is a homomorphism from 𝒢(𝒜) to 𝒢(ℬ).
</p>
<br>

<p class="fragment" align="left">
Thus, CSP(ℬ) ≤ CSP(𝒢(ℬ)), where the reduction maps an instance 𝒳 of CSP(ℬ) to the instance 𝒢(𝒳) of CSP(𝒢(ℬ)).
</p>
<br>

<!-- <p class="fragment" align="left"> -->
<!-- In particular, we may view CSP(ℬ) as the problem CSP(𝒢(ℬ)) where the instances are syntactically restricted,<br><br> -->
<!-- i.e., where some of the input relations are restricted to be graphs of operations. -->

## Review of some definitions

@ul

- A *quantifier-free pp-formula* in a signature 𝑆 is a conjunction of atomic 𝑆-formulas.<br>

- Two 𝑆-structures 𝒜 and ℬ over the same domain are *term-equivalent* if for every operation $f^𝒜$,<br> ∃ an 𝑆-term 𝑡 such that $f^𝒜(a) = t^ℬ(a)$ holds for every tuple $a$, and conversely for every operation $g^ℬ$ of ℬ.

@ulend


---

<!-- A *polymorphism* of a relational structure 𝒜 is a homomorphism ℎ : 𝒜ⁿ → 𝒜, for some 𝑛 ≥ 1.<br><br> -->

<!-- Alternatively, it is a map $h\colon A^n\to A$ such that for every relation $R^{\rel A}$ of $\rel A$ and all tuples $t_1,\dots,t_n\in R^{\rel A}$, the tuple $h(t_1,\dots,t_n)$ is in $R^{\rel A}$. -->


## Lemmas

**Lemma 1**. Let 𝑆 be a signature and ℬ be an 𝑆-structure.<br><br>
Let Σ be a finite set of identities such that ℬ ⊧ Σ.<br> 

<p class="fragment" align="left">
Then,
</p>
<br> 

<p class="fragment" align="left">
For every instance 𝒳 of CSP(ℬ), one can compute in poly-time an instance 𝒴 &nbsp; such that: 𝒴 ⊧ Σ and 𝒳 → ℬ iff 𝒴 → ℬ.
</p>
<br> 
<br> 

<p class="fragment" align="left">
**Lemma 2**.

Let 𝒜 and ℬ be term-equivalent structures, and suppose every relation of 𝒜 has a quantifier-free pp-definition in ℬ.
</p>

<p class="fragment" align="left">
Then CSP(𝒜) has a poly-time many-one reduction to CSP(ℬ).<br><br>
</p>

<p class="fragment" align="left">
As a corollary, we obtain an algebraic invariant for complexity of the CSP of finite structures.
</p>

---

Recall, a partial operation ℎ : 𝑅ⁿ ⇀ 𝐵 is a *partial polymorphism* if ∀ 𝑅 and 𝑡₁, …, 𝑡ₙ ∈ 𝑅, if ℎ(𝑡₁, …, 𝑡ₙ) is defined, then it is in 𝑅.<br>
<br>

<p class="fragment" align="left">
*Fact* (Romov, 1981). If 𝒜, ℬ are finite relational structures such that every partial polymorphism of ℬ is a partial polymorphism of 𝒜, then the relations of 𝒜 have quantifier-free pp-definitions in ℬ.
</p>
<br>

<p class="fragment" align="left">
**Corollary**.

For a finite structure ℬ, the complexity of CSP(ℬ) depends only on<br>

@ul

- the clone generated by the basic operations of ℬ and 

- the set of partial polymorphisms of the relational reduct of ℬ.

@ulend
</p>
<br>
<br>

<p class="fragment" align="left">
In the Boolean case, complexity of the CSP depends only on<br>

@ul

- the clone generated by the basic operations of ℬ and 

- the (total) polymorphisms of 𝒢(ℬ).

@ulend
</p>
<br>
<br>

<p class="fragment" align="left">
This invariant is weaker, in the sense that it contains less information.<br>
</p>
<br>
<p class="fragment" align="left">
 Indeed, every polymorphism of 𝒢(ℬ) is in particular a partial polymorphism of the relational reduct of ℬ.
</p>
<br>

<p class="fragment" align="left">
It is unclear whether this weaker invariant is enough to separate tractable and NP-complete problems for structures with bigger domains.
</p>

---

## Main Theorem
### a dichotomy for Boolean structures

<p class="fragment" align="left">
**Theorem**. Let ℬ be a Boolean structure.
</p>
<br>

<p class="fragment" align="left">
CSP(ℬ) is in P if<br>

@ul

- ℬ has an operation that is not essentially unary OR
- Pol(𝒢(ℬ)) has a polymorphism that is constant or not essentially unary;

@ulend
<br>
<br>

<p class="fragment" align="left">
otherwise CSP(ℬ) is NP-complete.
</p>
<br>

<p class="fragment" align="left">
The proof relies on the classical result of Post (1941).
</p>
<br>

<p class="fragment" align="left">
*Theorem* (Post). Let 𝑪 be a clone of operations on {0,1}.<br><br>

If 𝑪 contains an operation that is not essentially unary, then it contains one of the operations ∨, ∧, majority, or minority.<br><br>
</p>

---

## Proof of main theorem

If the Boolean structure ℬ has an operation that is not essentially unary, then by Post's theorem
<br><br>
the clone generated by the operations of ℬ has either a semilattice, majority, or minority operation.
<br>
<br>
<p class="fragment" align="left">
By Lemma 2, we can assume ℬ has such an operation in its language.
</p>
<br>
<p class="fragment" align="left">
We prove that the presence of such an operation puts CSP(ℬ) in P. 
</p>
<br>

<p class="fragment" align="left">
In fact, we prove the a stronger result in this case: <br><br>
For each finite 𝒳 &nbsp; there is at most a polynomial number of homomorphisms from 𝒳 &nbsp; to ℬ.
</p>

---

## The semilattice case

*Theorem 1*.<br><br>
If ℬ = ({0, 1}; 𝑠, …) is an 𝑆-structure where 𝑠(𝑥, 𝑦) = 𝑥 ∧ y or 𝑠(𝑥, 𝑦) = 𝑥 ∨ 𝑦, then CSP(ℬ) is in P.<br>
<br>

<p class="fragment" align="left">
*Proof*.<br><br>
Fix an 𝑆-structure 𝒳 = (𝑋; 𝑠, …), and recall, 𝒳 ' ⊧ Σ and 𝒳 → ℬ iff 𝒳 ' → ℬ.<br><br>

By Lemma 1 we can assume that 𝑠 is a semilattice operation.
</p>

<!-- We do the proof for 𝑠(𝑥, 𝑦) = 𝑥 ∧ 𝑦, the other case being similar. -->
<p class="fragment" align="left">
If we define 𝑥 ≤ 𝑦 iff 𝑥 = 𝑠(𝑥, 𝑦), then ∀ ℎ : 𝒜 → ℬ, the set $ℎ^{-1}(\{1\})$ is a principal filter in (𝑋, ≤) (upward closed set, closed under 𝑠).<br><br>
</p>

<p class="fragment" align="left">
Indeed, if 𝑥 ∈ $h^{-1}(\{1\})$ and 𝑥 ≤ y, then ℎ(𝑦) = 1 ∧ ℎ(𝑦) = ℎ(𝑥) ∧ ℎ(𝑦) = ℎ(𝑠(𝑥, 𝒚)) = ℎ(𝑥) = 1.<br><br>

Similarly, if 𝑥, 𝑦 ∈ $h^{-1}(\{1\})$, then ℎ(𝑠(𝒙, 𝑦)) = ℎ(𝒙) ∧ ℎ(𝒚) = 1.
</p>
<br>

<p class="fragment" align="left">
There are at most |𝑋| such principal filters, so there are at most |𝑋| homomorphisms 𝒳 → ℬ.
</p>
<br>

<p class="fragment" align="left">
To solve CSP(ℬ), it suffices to enumerate the linearly-many principal filters and check whether one of them induces a hom  𝒳 → ℬ.
</p>

---

## The majority case

*Theorem 2*.<br><br>
If ℬ = (\{0, 1\}; 𝑚, …) where 𝑚 is majority, then CSP(ℬ) is in P.
<br>

<p class="fragment" align="left">
*Proof*.<br><br> Fix an 𝑆-structure 𝒳 = (𝑋; 𝑚, …), so 𝑚 is a ternary operation.
</p>

<p class="fragment" align="left">
By Lemma 1 we can assume 𝑚 is the majority operation and satisfies 𝑚(𝑥₁, 𝑥₂, 𝑥₃) ≈ 𝑚(𝑥₁, 𝑥₃, 𝑥₂), since this identity holds in ℬ.
</p>
<br>

<p class="fragment" align="left">
Similarly, we can assume 𝑚 satisfies 𝑚(𝑎, 𝑚(𝑎, 𝑥, 𝑦), 𝑧) ≈ 𝑚(𝑎, 𝑥, 𝑚(𝑎, 𝑦, 𝑧)); i.e., ∀ 𝑎 ∈ 𝑋, (𝑥, 𝑦) ↦ 𝑚(𝑎, 𝑥, 𝑦) is a semilattice operation on 𝑋.
</p>
<br>

<p class="fragment" align="left">
Fix 𝑎 ∈ 𝑋, define 𝑥 ∧ 𝑦 := 𝑚(𝑎, 𝒙, 𝑦) and set 𝑥 ≤ 𝑦 iff 𝑥 = 𝑥 ∧ 𝑦.
</p>
<br>

<p class="fragment" align="left">
Suppose ℎ : 𝒳 → ℬ is a homomorphism that maps 𝑎 to 0.  Then the set $h^{-1}(\{1\})$ is a principal filter of (𝑋, ≤).
</p>
<br>

<p class="fragment" align="left">
Consequently, every homomorphism  𝒳 → ℬ mapping 𝑎 to 0 gives a principal filter, and one can enumerate<br><br> all the possible homomorphisms 𝒳 → ℬ by enumerating the linearly-many such filters.
</p>
<br>

<p class="fragment" align="left">
Fix 𝑎 ∈ 𝑋, define 𝑥 ∧ y := 𝑚(𝑎, 𝑥, 𝑦) and set 𝑥 ≤ 𝑦 iff 𝑥 = 𝑥 ∧ 𝑦.
<br>

<p class="fragment" align="left">
Suppose ℎ : 𝒳 → ℬ is a homomorphism that maps 𝑎 to 0.  Then the set $h^{-1}(\{1\})$ is a principal filter of (𝑋, ≤).
</p>
<br>

<p class="fragment" align="left">
Consequently, every homomorphism  𝒳 → ℬ mapping 𝑎 to 0 gives a principal filter.
</p>
<br>

<p class="fragment" align="left">
One can enumerate all the possible homomorphisms 𝒳 → ℬ by enumerating the linearly-many such filters.
</p>

---

## The affine case

*Theorem 3*.<br><br>
If ℬ = (\{0, 1\}; 𝑚, …), where 𝑚(𝑥, 𝑦, 𝑧) = 𝑥 + 𝑦 + 𝑧, then CSP(ℬ) is in P.
<br><br>

<p class="fragment" align="left">
*Proof*.<br>
<br>
Fix an 𝑆-structure 𝒳 = (𝑋; 𝑚, …), so 𝑚 is a ternary operation.
</p>
<br>

<p class="fragment" align="left">
As above, Lemma 1 yields an 𝑚 satisfying all the necessary properties that we use below.
</p>
<br>

<p class="fragment" align="left">
Fix 𝑎 ∈ 𝑋 and define 𝑥 + 𝑦 := 𝑚(𝑎, 𝑥, 𝑦).
</p>
<br>

<p class="fragment" align="left">
The algebra (𝑋; +, 𝑎) is a vector space over the two-element field 𝔽₂, for which we
can compute a basis \{𝑎₁, …, 𝑎ₖ\} where 𝑘 is logarithmic in |𝑋|.
</p>
<br>

<p class="fragment" align="left">
Every homomorphism ℎ: 𝒳 → ℬ mapping 𝑎 to 0 is then an 𝔽₂-linear map between (𝑋; +, 𝑎) and (\{0,1\}; +,0),<br>
<br>
and is thus determined by the values ℎ(𝑎₁), …, ℎ(𝑎ₖ).
</p>
<br>

<p class="fragment" align="left">
This gives a linear number of possible homomorphisms ℎ : 𝒳 → ℬ mapping 𝑎 to 0,<br>
<br>
and thus at most a quadratic number of possible homomorphisms 𝒉: 𝒳 → ℬ.
</p>

---

## Proof of the Main Theorem

*Theorem*.<br><br>
If ℬ is a Boolean structure with finitely many relation and operation symbols, then CSP(ℬ) is in P or is NP-complete 
<br>
<br>

<p class="fragment" align="left">
*Proof*.<br>
<br>
*Case 1*. ℬ has an operation that is not essentially unary.  
</p>
<br>

<p class="fragment" align="left">
Then CSP(ℬ) is in P by Theorems 1, 2, 3.
</p>
<br>

<p class="fragment" align="left">
*Case 2*. ℬ has only essentially unary operations.  
</p>
<br>

<p class="fragment" align="left">
Then ℬ is term-equivalent to a ℬ' whose operations are all unary, and CSP(ℬ) and CSP(ℬ') are poly-time equivalent by Lemma 2.
</p>
<br>

<p class="fragment" align="left">
By the FMS Theorem, CSP(ℬ') and CSP(𝒢(ℬ')) are poly-time equivalent.
</p>
<br>

<p class="fragment" align="left">
By Schaefer's dichotomy theorem, CSP(𝒢(ℬ')) is in P if Pol(𝒢(ℬ')) contains an operation that is not essentially unary,<br><br> and is NP-complete otherwise.
</p>
<br>

<p class="fragment" align="left">
Finally, Pol(𝒢(ℬ')) and Pol(𝒢(ℬ)) are the same clone, which yields the result.
</p>

---

## Example 

A structure ℬ with CSP(𝒢(ℬ)) ≰ CSP(ℬ)
<br>
<br>

<p class="fragment" align="left">
Define ℬ = (\{0, 1\}; ·) where · is the binary operation: 𝑥 · 𝑦 = 1  iff 𝑥 = 𝑦 = 0.
</p>
<br>

<p class="fragment" align="left">
The relation induced by · is 𝑅 = \{(0,0,1), (0,1,0), (1,0,0), (1,1,0)\}. 
</p>
<br>

<p class="fragment" align="left">
None of the operations ∨, ∧, maj, min is a polymorphism of 𝒢(ℬ) = (\{0, 1\}, 𝑅), and
</p>
<br>

<p class="fragment" align="left">
a constant map into \{0, 1\} is not an endomorphism of ℬ.
</p>
<br>

<p class="fragment" align="left">
Therefore, CSP(𝒢(ℬ)) is NP-hard by Schaefer's theorem.
</p>
<br>

<p class="fragment" align="left">
On the other hand, the operation (𝑥, 𝑦) ↦ (𝑥 · 𝑦) · (𝑥 · 𝑦) is exactly 𝑥 ∨ 𝑦.
</p>
<br>

<p class="fragment" align="left">
So the above results imply CSP(ℬ) ∈ P.
</p>

<!-- Note: In fact, since the clone generated by · contains all ops from    -->
<!-- Thms 1, 2, 3, any of the algorithms above could be used to solve CSP(ℬ). -->

---

## Future Work

### Open Questions

Our results raise a number of questions for developing a general theory of CSP of arbitrary finite structures.

@ul

- The reduction from CSPs of relational structures to CSPs of algebras in Feder, et al is a poly-time Turing reduction.<br><br>*Does a poly-time many-one reduction exist?*<br><br>

- We showed that for a finite structure ℬ the complexity of CSP(ℬ) depends only on the clone generated by the operatios of ℬ and the partial polymorphisms of the relational reduct of ℬ.<br><br> *Is the complexity of CSP(ℬ) captured by the polymorphisms of the relational reduct of ℬ?*<br><br>

- For the Boolean dichotomy above the crucial step is establishing that as long as ℬ has an operation that is not essentially unary, then for all finite 𝒜, there are only polynomially many homomorphisms from 𝒜 to ℬ.<br><br>*It is in general unknown what other properties of finite algebras imply such a polynomial bound, and this question is interesting also from a purely algebraic perspective.*

@ulend


---

## Questions relating to width

Our width results for general Boolean structures show a striking difference compared to finite relational structures.<br><br> Here are some questions for developing a general theory of CSPs of structures: 

@ul

- Can one characterize bounded (relational) width algebraically?<br><br>

- If ℬ has bounded width, does it have width (2,ℓ) for ℓ large enough?<br><br>

- Are there 𝑘, ℓ such that every structure ℬ with bounded relational width has relational width 𝑘, ℓ?

@ulend

---

## Thank you!
